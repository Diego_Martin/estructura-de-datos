#ifndef __RANDOM_CHOICE_H__
#define __RANDOM_CHOICE_H__

#include "../init.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

namespace Random {
    Object *Choice (List *list) {
        srand (time (NULL));
        int res = rand () % list->length ();
        return ((*list) [res]);
    }

    Object *Choice (Tuple *list) {
        srand (time (NULL));
        int res = rand () % list->length ();
        return ((*list) [res]);
    }
}

#endif