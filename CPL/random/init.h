#ifndef __RANDOM_H__
#define __RANDOM_H__

#include "../init.h"
#include "./randint.cpp"
#include "./choice.cpp"

namespace Random {
    Integer *Randint (Integer *min, Integer *max);
    Object *Choice (List *list);
    Object *Choice (Tuple *list);
}

#endif