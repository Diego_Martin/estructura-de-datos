#ifndef __RANDOM_RANDINT_H__
#define __RANDOM_RANDINT_H__

#include "../init.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

namespace Random {
    Integer *Randint (Integer *min, Integer *max) {
        srand (time (NULL));
        int res = rand () % (max->__getcvalue__ () - min->__getcvalue__ () + 1) + min->__getcvalue__ ();
        return new Integer (res);
    }
}

#endif