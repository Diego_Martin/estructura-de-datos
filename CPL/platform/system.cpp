#ifndef __PLATFORM_SYSTEM_H__
#define __PLATFORM_SYSTEM_H__

#include "../init.h"


#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)
	#define SO "Windows"
#elif __APPLE__
	#if TARGET_IPHONE_SIMULATOR
		#define SO "Simulador IOS"
	#elif TARGET_OS_IPHONE
		#define SO "IOS"
	#elif TARGET_OS_MAC
		#define SO "Mac OS"
	#else
		#define SO "Apple desconocido"
	#endif
#elif __linux__
	#define SO "Linux"
#elif __unix__ // all unices not caught above
	#define SO "Unix"
#elif defined(_POSIX_VERSION)
	#define SO "Posix"
#else
	#define SO "Compilador desconocido"
#endif


namespace Platform {
    String *System () {
        return new String (SO);
    }
}

#endif