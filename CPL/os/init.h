#ifndef __OS_H__
#define __OS_H__

#include "./system.cpp"
#include "./getcwd.cpp"
#include "./getpid.cpp"

// Funciones
namespace Os {
    String *System ();
    String *Getcwd ();
    Integer *Getpid ();
}

#endif