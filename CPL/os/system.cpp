#ifndef __OS_SYSTEM_H__
#define __OS_SYSTEM_H__

#include "../basics/init.h"
#include <stdlib.h>

namespace Os {
    String *System (String *comando) {
        return new String (system (comando->__repr__ ()));
    }
}

#endif