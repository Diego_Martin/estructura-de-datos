#ifndef __OS_GETCWD_H__
#define __OS_GETCWD_H__

#include "../basics/init.h"
#include "../sys/init.h"
#include <limits.h>
#include <unistd.h>

namespace Os {
    String *Getcwd () {
        char cwd[PATH_MAX];
        if (getcwd (cwd, sizeof (cwd)) == NULL) {
            Sys::Exit (new String ("Error en 'Os::Getcwd()'. No se ha podido obtener el directorio de trabajo actual (cwd)."));
        }

        return new String (cwd);
    }
}

#endif