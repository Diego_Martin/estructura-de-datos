#ifndef __OS_GETPID_H__
#define __OS_GETPID_H__

#include "../basics/init.h"
#include "../sys/init.h"

#include <unistd.h>

namespace Os {
    Integer *Getpid () {
        return new Integer ((int) getpid ());
    }
}

#endif