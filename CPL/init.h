#ifndef __CPL_H__
#define __CPL_H__

#define __eq__   operator==
#define __le__   operator<=
#define __ge__   operator>=
#define __lt__   operator<
#define __gt__   operator>
#define __not__  operator!
#define __hash__ operator[]
#define __call__ operator()
#define __add__  operator+
#define __subs__ operator-
#define __mul__  operator*
#define __div__  operator/
#define __and__  operator&&
#define __or__   operator||
#define __as__   operator=
// #define . ::
#define NotImplemented(x) notImplemented(x, __PRETTY_FUNCTION__, __FILE__, __LINE__)

#include <stdio.h>
#include <stdlib.h>

void notImplemented (const char *msg = "No está implementado aún.", const char *fun = __PRETTY_FUNCTION__, const char *file = __FILE__, int line = __LINE__) {
	printf ("NotImplemented function '%s' at file '%s', line %i: %s\n", fun, file, line, msg);
	exit (EXIT_FAILURE);
}

#include "core/interfaces/init.h"
#include "core/clases/init.h"
#include "core/excepciones/init.h"
#include "basics/init.h"

#endif