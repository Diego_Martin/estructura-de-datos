#ifndef __ITERABLE_H__
#define __ITERABLE_H__

#include "Object.h"
#include "../clases/Integer.h"

class Iterable : public Object
{
	public:
		bool __is_iterable__ = true;
		virtual unsigned long long int length () = 0;
		virtual void each () = 0;
		virtual bool has_next () = 0;
		virtual Object *next () = 0;
		virtual Object *get () = 0;
		virtual Object *__hash__ (Object *index) = 0;
		virtual Object *slice (Integer *start, Integer *end = new Integer (-1), Integer *step = new Integer (1)) = 0;

	protected:
		int __index__;
};

#endif