#ifndef __LIST_H__
#define __LIST_H__

#include "../interfaces/Object.h"
#include "../interfaces/Iterable.h"
#include "Integer.h"
// #include "IndexOutOfBoundsException.h"
#include "NoneType.h"
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sstream>
using namespace std;

class List : public Object//, public Iterable
{
	public:
		List () {
			this->len = 0;
			this->valor = (Object **) malloc (sizeof (Object *) * this->len);
		}

		List (Object *item, ...) {
			Object *s;
			this->len = 0;
			va_list lista;
			va_start (lista, item);

			for (s = item; s != NULL; s = va_arg (lista, Object *)) {
				this->append (s);
			}

			va_end (lista);
		}

		~List () {
			free (this->valor);
		}

		const char *__repr__ () { // DONEEEE
			stringstream canal;
			canal << "[";
			for (int i = 0; i < this->len; i++) {
				if (strcmp (this->operator[] (i)->__name__(), "String") == 0)
					canal << "'";

				canal << this->operator[] (i)->__repr__ ();

				if (strcmp (this->operator[] (i)->__name__(), "String") == 0)
					canal << "'";
				
				if (i < this->len - 1)
					canal << ", ";
			}
			canal << "]";
			const string& res = canal.str ();
			char *ret = (char *) malloc (sizeof (char) * strlen (res.c_str ()));
			strcpy (ret, res.c_str ());
			return (const char *) ret;
		}

		const char *__name__ () {
			return "List";
		}

		unsigned long long int __sizeof__ () {
			return sizeof (Object *) * this->len;
		}

		unsigned long long int length () {
			return this->len;
		}

		// Object *operator [] (int index) {
		// 	if (index < this->len && index > -1)
		// 		return *(valor + index);
		// 	else if (index >= (-1 * this->len) && index < 0)
		// 		return *(valor + (-1 * index));
		// 	else
		// 		throw new IndexOutOfBoundsException (index, this->length ());
		// }

		Object *__hash__ (long long int index) {
			if (index < this->len && index > -1)
				return *(valor + index);
			else if (index >= (-1 * this->len) && index < 0)
				return *(valor + (-1 * index));
			else
				throw 2;//new IndexOutOfBoundsException (index, this->length ());
		}

		Object *__hash__ (Object *index) {
			if (strcmp (index->__name__ (), "Integer") == 0)
				return (*this) [((Integer *) index)->__getcvalue__ ()];
				// return &this [*((int *) index->__cvalue__ ())];
			else
				throw 3;
		}

		NoneType *append (Object *elemento) {
			this->__grow__ ();
			*(this->valor + this->len - 1) = elemento;
			return new NoneType ();
		}

		NoneType *append (Object *elemento, Object *elemento2) {
			this->__grow__ (2);
			*(this->valor + this->len - 2) = elemento;
			*(this->valor + this->len - 1) = elemento2;
			return new NoneType ();
		}

		NoneType *extend (List *elemento) {
			for (int i = 0; i < elemento->length (); i++) {
				this->append (elemento->operator[] (i));
			}
			return new NoneType ();
		}

		// List *__mro__ () {
		// 	List *ret = Object::__mro__ ();
		// 	ret->append (new String (this->__name__ ()));
		// 	return ret;
		// }

	private:
		Object **valor;
		unsigned long long int len;

		void __grow__ (int size = 1) {
			this->len += size;
			this->valor = (Object **) realloc (this->valor, sizeof (Object *) * this->len);
		}
};

#endif