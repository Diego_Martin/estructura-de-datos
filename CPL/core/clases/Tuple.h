#ifndef __TUPLE_H__
#define __TUPLE_H__

#include "../interfaces/Object.h"
#include "List.h"
#include "NoneType.h"
#include "Integer.h"
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sstream>

class Tuple : public Object
{
    public:
        Tuple () {
            // this->value = new List ();
            this->len = 0;
        }

        Tuple (Object *item, ...) {
            Object *s;
            this->len = 0;
            va_list lista;
            va_start (lista, item);
            
            for (s = item; s != NULL; s = va_arg (lista, Object *)) {
                this->value.append (s);
                this->len++;
            }

            va_end (lista);
        }

        const char *__repr__ () { // DONEEEE
			stringstream canal;
			canal << "(";
			for (int i = 0; i < this->len; i++) {
				if (strcmp (this->operator[] (i)->__name__(), "String") == 0)
					canal << "'";

				canal << this->operator[] (i)->__repr__ ();

				if (strcmp (this->operator[] (i)->__name__(), "String") == 0)
					canal << "'";
				
				if (i < this->len - 1)
					canal << ", ";
			}
			canal << ")";
			const string& res = canal.str ();
			char *ret = (char *) malloc (sizeof (char) * strlen (res.c_str ()));
			strcpy (ret, res.c_str ());
			return (const char *) ret;
		}

        Object *__hash__ (int index) {
			return this->value [index];
		}

        Object *__hash__ (Object *index) {
			if (strcmp (index->__name__ (), "Integer") == 0)
				return &this [*((int *) index->__cvalue__ ())];
			else
				throw 3;
		}

        unsigned long long int length () {
            return this->len;
        }
    
    private:
        List value;
        unsigned long long int len;

};

#endif