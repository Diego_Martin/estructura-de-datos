#ifndef __CORE_CLASSES_H__
#define __CORE_CLASSES_H__

#include "Boolean.h"
#include "Dict.h"
#include "File.h"
#include "Float.h"
#include "Integer.h"
#include "List.h"
#include "NoneType.h"
#include "Set.h"
#include "String.h"
#include "Tuple.h"
#include "Type.h"

#endif