#ifndef __DICT_H__
#define __DICT_H__

#include "../interfaces/Object.h"
#include "../interfaces/Iterable.h"
#include "String.h"
#include "List.h"
#include "NoneType.h"
// #include "IndexNotInDictException.h"
// #include "IndexAlreadyExistsException.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sstream>

class Dict : public Object //, public Iterable
{
	public:
		// Dict () {}
		~Dict () {
			delete &this->valor;
			delete &this->clave;
		}

		const char *__repr__ () {
			stringstream canal;
			canal << "{";
			for (int i = 0; i < this->valor.length (); i++) {
				canal << ((strcmp (this->clave [i]->__name__ (), "String") == 0) ? "'" : "") << this->clave [i]->__repr__ () << ((strcmp (this->clave [i]->__name__ (), "String") == 0) ? "'" : "") << ": " << ((strcmp (this->valor [i]->__name__ (), "String") == 0) ? "'" : "") << this->valor [i]->__repr__ () << ((strcmp (this->valor [i]->__name__ (), "String") == 0) ? "'" : "");

				if (i < this->valor.length () - 1)
					canal << ", ";
			}
			canal << "}";
			const string& res = canal.str ();
			String *ret = new String (res.c_str ());
			return ret->__repr__ ();
		}

		const char *__name__ () {
			return "Dict";
		}

		void __setstr__ (const char *frase) {
			return;
		}

		int length () {
			return this->valor.length ();
		}

		unsigned long long int __sizeof__ () {
			return sizeof (*this);
		}

		Object *operator [] (Object *index) {
			bool encontrado = false;
			int i = 0;
			for (; i < this->clave.length (); i++)
				if (strcmp (this->clave[i]->__repr__ (), index->__repr__ ()) == 0 && strcmp (this->clave[i]->__name__ (), index->__name__ ()) == 0) {
					encontrado = true;
					break;
				}
			
			if (!encontrado)
				throw 32;// new IndexNotInDictException (index, this, this);

			return this->valor [i];
		}

		NoneType *update (Object *key, Object *valor) {
			// printf("En el update\n");
			// for (int i = 0; i < this->clave.length (); i++) {
			// 	printf("Iterador, i: %i\n", i);
			// 	printf("Condicion: %s\n", (strcmp (this->clave [i]->__repr__ (), key->__repr__ ()) == 0) ? "True" : "False");
			// 	if (strcmp (this->clave [i]->__repr__ (), key->__repr__ ()) == 0) {
			// 		printf("En el if\n");
			// 		throw new IndexAlreadyExistsException (valor, this);
			// 	}
			// }
			
			this->clave.append (key);
			this->valor.append (valor);
			return new NoneType ();
		}

		List *keys () {
			return &(this->clave);
		}

		List *values () {
			return &(this->valor);
		}

	private:
		List valor;
		List clave;
};

#endif