#ifndef __NoneType_H__
#define __NoneType_H__

#include "../interfaces/Object.h"
#include "List.h"
#include <stdlib.h>

class NoneType : public Object
{
	public:
		NoneType () {
			this->valor = NULL;
		}

		~NoneType () {}

		const char *__repr__ () {
			return "None";
		}

		const char *__name__ () {
			return "NoneType";
		}

		unsigned long long int __sizeof__ () {
			return sizeof (this->valor);
		}

		Object * __call__ () {
			Object *res (NULL);
			return res;
		}

		static NoneType *nada () {
			return new NoneType ();
		}

		NoneType *__cvalue__ () {
			return this;
		}

		// List *__mro__ () {
		// 	List *ret = Object::__mro__ ();
		// 	ret->append (new String (this->__name__ ()));
		// 	return ret;
		// }

	private:
		void *valor;
};

NoneType *NoneType= new NoneType ();

#endif