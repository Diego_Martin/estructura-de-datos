#ifndef __WEBBROWSER_H__
#define __WEBBROWSER_H__

namespace Webbrowser {
    class Navigator : public Object {
        public:
            Navigator (String *path);
            ~Navigator ();
            const char *__name__ ();
            NoneType *set_path (String *path);
            NoneType *open (String *url);

        private:
            char *ruta;
    };
}

#include "../init.h"
#include "./navigator.cpp"

namespace Webbrowser {
    Navigator *Chrome = new Navigator (new String ("/mnt/c/Program Files/Google/Chrome/Application/chrome.exe"));
    Navigator *Firefox = new Navigator (new String ("/mnt/c/Program Files/Mozilla Firefox/firefox.exe"));
    Navigator *Get (String *path);
}

#endif