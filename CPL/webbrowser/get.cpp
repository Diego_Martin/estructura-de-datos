#ifndef __WEBBROWSER_GET__
#define __WEBBROWSER_GET__

#include "../init.h"

namespace Webbrowser {
    Navigator *Get (String *path) {
        return new Navigator (path);
    }
}

#endif