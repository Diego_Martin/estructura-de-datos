#ifndef __WEBBROWSER_NAVIGATOR_H__
#define __WEBBROWSER_NAVIGATOR_H__

#include "../core/interfaces/init.h"
#include "../core/clases/init.h"
#include "../os/init.h"

#include <stdlib.h>
#include <string.h>


Webbrowser::Navigator::Navigator (String *path = new String ("/mnt/c/Program Files/Google/Chrome/Application/chrome.exe")) {
    this->ruta = (char *) malloc (sizeof (char) * path->length ());
    strcpy (this->ruta, path->__repr__ ());
}

Webbrowser::Navigator::~Navigator () {
    free (this->ruta);
}

const char *Webbrowser::Navigator::__name__ () {
    return "Navigator";
}

NoneType *Webbrowser::Navigator::set_path (String *path) {
    free (this->ruta);
    this->ruta = (char *) malloc (sizeof (char) * path->length ());
    strcpy (this->ruta, path->__repr__ ());
    return new NoneType ();
}

NoneType *Webbrowser::Navigator::open (String *url) {
    char *com = (char *) malloc (sizeof (char *) * (strlen (this->ruta) + strlen (url->__repr__ ()) + 4));
    strcpy (com, "'");
    strcat (com, this->ruta);
    strcat (com, "' ");
    strcat (com, url->__repr__ ());
    // strcat (com, "\0");
    String *command = new String (com);
    Os::System (command);
    // printf ("%s\n", command->__repr__ ());
    return new NoneType ();
}

#endif