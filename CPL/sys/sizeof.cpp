#ifndef __SYS_SIZEOF_H__
#define __SYS_SIZEOF_H__

#include "../init.h"
#include <stdio.h>
#include <stdlib.h>

namespace Sys {
    Integer *Sizeof (String *variable) {
		return new Integer (variable->__sizeof__ ());
	}

	Integer *Sizeof (Integer *variable) {
		return new Integer (variable->__sizeof__ ());
	}

	Integer *Sizeof (Boolean *variable) {
		return new Integer (variable->__sizeof__ ());
	}

	Integer *Sizeof (Float *variable) {
		return new Integer (variable->__sizeof__ ());
	}

	Integer *Sizeof (NoneType *variable) {
		return new Integer (variable->__sizeof__ ());
	}

	Integer *Sizeof (File *variable) {
		return new Integer (variable->__sizeof__ ());
	}

	Integer *Sizeof (List *variable) {
		return new Integer (variable->__sizeof__ ());
	}

	Integer *Sizeof (Dict *variable) {
		return new Integer (variable->__sizeof__ ());
	}
}

#endif