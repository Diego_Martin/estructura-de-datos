#ifndef __SYS_EXIT_H__
#define __SYS_EXIT_H__

#include "../init.h"
#include <stdio.h>
#include <stdlib.h>

namespace Sys
{
    NoneType *Exit (Integer *estado = new Integer (0)) {
		exit (estado->__getcvalue__ ());
	}

	NoneType *Exit (String *mensaje, File *canal = Sys::Stdout) {
		// printf ("%s\n", mensaje->__repr__ ());
		fprintf (canal->__cvalue__ (), "%s\n", mensaje->__repr__ ());
		exit (0);
	}
}

#endif