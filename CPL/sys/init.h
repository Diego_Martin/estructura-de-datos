#ifndef __SYS_H__
#define __SYS_H__

// Constantes
namespace Sys
{
	File *Stdin  = new File (stdin);
	File *Stdout = new File (stdout);
	File *Stderr = new File (stderr);
	File *Stdvbs = new File (stdout);
}

#include "../init.h"
#include "./exit.cpp"
#include "./sizeof.cpp"
#include <stdio.h>
#include <stdlib.h>

// Funciones
namespace Sys
{
	NoneType *Exit (Integer *estado);
	NoneType *Exit (String *mensaje, File *canal);

	Integer *Sizeof (String *variable);
	Integer *Sizeof (Integer *variable);
	Integer *Sizeof (Boolean *variable);
	Integer *Sizeof (Float *variable);
	Integer *Sizeof (NoneType *variable);
	Integer *Sizeof (File *variable);
	Integer *Sizeof (List *variable);
	Integer *Sizeof (Dict *variable);
};

#endif