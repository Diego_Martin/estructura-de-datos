#ifndef __PRINT_H__
#define __PRINT_H__

#include "../core/interfaces/init.h"
#include "../core/clases/init.h"
#include "../core/excepciones/init.h"
#include "../sys/init.h"
#include <stdio.h>


NoneType *print (Object *variable, String *end = new String ("\n"), File *file = Sys::Stdout, Boolean *flush = new Boolean (true)) {
	if (end == NULL)
		end = new String ("\n");

	if (file == NULL)
		file = Sys::Stdout;

	if (flush == NULL)
		flush = new Boolean (true);

	if (file == Sys::Stdin) {
		fprintf (stderr, "\033[31;40mERROR: No se puede imprimir '%s' en el canal 'Sys::Stdin' (C: stdin) porque 'Sys::Stdin' es un canal de entrada de datos, no de salida.\033[97;40m\n\n", variable->__name__ ());
		Sys::Exit (new Integer (1));
	}

	else if (file == Sys::Stdout)
		fprintf (file->__cvalue__ (), "\033[97;40m%s%s\033[97;40m", variable->__repr__ (), end->__repr__ ());

	else if (file == Sys::Stderr)
		fprintf (file->__cvalue__ (), "\033[31;40m%s%s\033[97;40m", variable->__repr__ (), end->__repr__ ());

	else if (file == Sys::Stdvbs)
		fprintf (file->__cvalue__ (), "\033[32;40m%s%s\033[97;40m", variable->__repr__ (), end->__repr__ ());
	
	else {
		char *cosa = (char *) malloc (sizeof(char) * (strlen (variable->__repr__ ()) + strlen (end->__repr__ ())));
		strcpy (cosa, variable->__repr__ ());
		strcat (cosa, end->__repr__ ());
		file->append (new String (cosa));
		free (cosa);
	}

	if (flush->__getcvalue__ ())
		fflush (file->__cvalue__ ());

	return new NoneType ();
}

NoneType *print (Object *variable, Object *variable2, String *sep = new String (" "), String *end = new String ("\n"), File *file = Sys::Stdout, Boolean *flush = new Boolean (true)) {
	if (sep == NULL)
		sep = new String (" ");

	if (end == NULL)
		end = new String ("\n");

	if (file == NULL)
		file == Sys::Stdout;

	if (flush == NULL)
		flush = new Boolean (true);

	if (file == Sys::Stdin) {
		fprintf (stderr, "\033[31;40mERROR: No se puede imprimir '%s' en el canal 'Sys::Stdin' (C: stdin) porque 'Sys::Stdin' es un canal de entrada de datos, no de salida.\033[97;40mn\n", variable->__name__ ());
		Sys::Exit (new Integer (1));
	}

	else if (file == Sys::Stdout)
		fprintf (file->__cvalue__ (), "\033[97;40m%s%s%s%s\033[97;40m", variable->__repr__ (), sep->__repr__ (), variable2->__repr__ (), end->__repr__ ());

	else if (file == Sys::Stderr)
		fprintf (file->__cvalue__ (), "\033[31;40m%s%s%s%s\033[97;40m", variable->__repr__ (), sep->__repr__ (), variable2->__repr__ (), end->__repr__ ());

	else if (file == Sys::Stdvbs)
		fprintf (file->__cvalue__ (), "\033[32;40m%s%s%s%s\033[97;40m", variable->__repr__ (), sep->__repr__ (), variable2->__repr__ (), end->__repr__ ());
	
	else {
		char *cosa = (char *) malloc (sizeof (char) * (strlen (variable->__repr__ ()) + strlen (variable2->__repr__ ()) + strlen (sep->__repr__ ()) + strlen (end->__repr__ ())));
		strcpy (cosa, variable->__repr__ ());
		strcat (cosa, sep->__repr__ ());
		strcat (cosa, variable2->__repr__ ());
		strcat (cosa, end->__repr__ ());
		file->append (new String (cosa));
		free (cosa);
	}

	if (flush->__getcvalue__ ())
		fflush (file->__cvalue__ ());

	return new NoneType ();
}

NoneType *print (Object *variable, Object *variable2, Object *variable3, String *sep = new String (" "), String *end = new String ("\n"), File *file = Sys::Stdout, Boolean *flush = new Boolean (true)) {
	if (sep == NULL)
		sep = new String (" ");

	if (end == NULL)
		end = new String ("\n");

	if (file == NULL)
		file == Sys::Stdout;

	if (flush == NULL)
		flush = new Boolean (true);

	if (file == Sys::Stdin) {
		fprintf (stderr, "\033[31;40mERROR: No se puede imprimir '%s' en el canal 'Sys::Stdin' (C: stdin) porque 'Sys::Stdin' es un canal de entrada de datos, no de salida.\033[97;40m\n\n", variable->__name__ ());
		Sys::Exit (new Integer (1));
	}

	else if (file == Sys::Stdout)
		fprintf (file->__cvalue__ (), "\033[97;40m%s%s%s%s%s%s\033[97;40m", variable->__repr__ (), sep->__repr__ (), variable2->__repr__ (), sep->__repr__ (), variable3->__repr__ (), end->__repr__ ());

	else if (file == Sys::Stderr)
		fprintf (file->__cvalue__ (), "\033[31;40m%s%s%s%s%s%s\033[97;40m", variable->__repr__ (), sep->__repr__ (), variable2->__repr__ (), sep->__repr__ (), variable3->__repr__ (), end->__repr__ ());

	else if (file == Sys::Stdvbs)
		fprintf (file->__cvalue__ (), "\033[32;40m%s%s%s%s%s%s\033[97;40m", variable->__repr__ (), sep->__repr__ (), variable2->__repr__ (), sep->__repr__ (), variable3->__repr__ (), end->__repr__ ());
	
	else {
		char *cosa = (char *) malloc (sizeof (char) * (strlen (variable->__repr__ ()) + strlen (variable2->__repr__ ()) + strlen (variable3->__repr__ ()) + (strlen (sep->__repr__ ()) * 2) + strlen (end->__repr__ ())));
		strcpy (cosa, variable->__repr__ ());
		strcat (cosa, sep->__repr__ ());
		strcat (cosa, variable2->__repr__ ());
		strcat (cosa, sep->__repr__ ());
		strcat (cosa, variable3->__repr__ ());
		strcat (cosa, end->__repr__ ());
		file->append (new String (cosa));
		free (cosa);
	}

	if (flush->__getcvalue__ ())
		fflush (file->__cvalue__ ());

	return new NoneType ();
}

#endif