#ifndef __INPUT_H__
#define __INPUT_H__

#include "../core/interfaces/init.h"
#include "../core/clases/init.h"
#include "../core/excepciones/init.h"
#include "../sys/init.h"
#include <stdio.h>

String *input (String *prompt = new String (""), Boolean *flush = new Boolean (true)) {
	char *res;
	print (prompt, new String (""));
	scanf (" %s", res);

	if (flush->__getcvalue__ ())
		fflush (Sys::Stdin->__cvalue__ ());
	
	return new String (res);
}

#endif