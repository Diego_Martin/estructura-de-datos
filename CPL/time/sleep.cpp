#ifndef __TIME_SLEEP_H__
#define __TIME_SLEEP_H__

// #include "../os/init.h"
#include "../init.h"
#include "./time.h"

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)
	//define something for Windows (32-bit and 64-bit, this part is common)
	#define SO "Windows"
	#define MiWindows
	#include <windows.h>
	#ifdef _WIN64
		//define something for Windows (64-bit only)
		#define ARQUITECTURA "x64"
	#else
		//define something for Windows (32-bit only)
		#define ARQUITECTURA "x32"
	#endif
#elif __APPLE__
	#include <TargetConditionals.h>
	#if TARGET_IPHONE_SIMULATOR
		// iOS Simulator
		#define SO "Simulador IOS"
		#define ARQUITECTURA ""
	#elif TARGET_OS_IPHONE
		// iOS device
		#define SO "IOS"
		#define ARQUITECTURA ""
	#elif TARGET_OS_MAC
		// Other kinds of Mac OS
		#define SO "Mac OS"
		#define ARQUITECTURA ""
	#else
		#define SO "Apple desconocido"
		#define ARQUITECTURA ""
	#endif
#elif __linux__
	#define MiLinux
	#include <unistd.h>
	#define SO "Linux"
	#define ARQUITECTURA ""
#elif __unix__ // all unices not caught above
	#define MiUnix
	#include <unistd.h>
	#define SO "Unix"
	#define ARQUITECTURA ""
#elif defined(_POSIX_VERSION)
	#define MiPosix
	#include <unistd.h>
	#define SO "Posix"
	#define ARQUITECTURA ""
#else
	#define SO "Compilador desconocido"
	#define ARQUITECTURA ""
#endif

namespace Time {
	NoneType *Sleep (Float *segundos = new Float (0)) {
		#ifdef MiWindows
			Sleep (((int) segundos->__getcvalue__ ()) * 1000);
		#elif defined MiLinux
			usleep (segundos->__getcvalue__ () * 1000000);
		#endif
		return None;
	}

	NoneType *Sleep (Integer *segundos = new Integer (0)) {
		#ifdef MiWindows
			Sleep (((int) segundos->__getcvalue__ ()) * 1000);
		#elif defined MiLinux
			usleep (segundos->__getcvalue__ () * 1000000);
		#endif
		return None;
	}
}

#endif