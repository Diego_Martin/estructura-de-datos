#ifndef __TIME_H__
#define __TIME_H__

#include "../init.h"
#include "./sleep.cpp"

namespace Time {
    NoneType *Sleep (Float *segundos);
    NoneType *Sleep (Integer *segundos);
}

#endif