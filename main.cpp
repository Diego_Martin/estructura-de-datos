#include "CPL/init.h"
#include "CPL/os/init.h"
#include "CPL/platform/init.h"
#include "CPL/random/init.h"
#include "CPL/time/init.h"
#include "CPL/webbrowser/init.h"
#include <stdio.h>

class Prueba : public Object
{
public:
	void si () {
		printf ("Si\n");
	}

	const char *__name__ () {
		return "Prueba";
	}
	// Prueba();
	// ~Prueba();
	
};



void si () {
	NotImplemented ("No programado aun");
}



int main (int argc, char const *argv[])
{
	Prueba *prueba = new Prueba ();
	prueba->si ();
	List *array = new List ();
	array->append (new String ("Holá Jose Luis"));
	array->append (new Integer (73));
	array->append (True);
	array->append (new Float (3.14));
	print (array);
	printf ("%llu\n", array->length ());
	for (int i = 0; i < array->length (); i++)
		print ((*array) [i]);

	print (new String ("Hola"), new Integer (3), new Boolean (true));
	print (prueba);
	File *archivo = new File ("./CPL/archivo.txt");
	array->append (prueba, archivo);
	// print (len ((Iterable *) (*array) [2])); // No funciona len ()
	// printf ("%llu\n", ((String *) ((*array) [0]))->length ());
	print (new String ("El fichero 'archivo' ocupa:"), Sys::Sizeof (archivo));
	print (array);
	((Prueba *)((*array) [4]))->si ();
	Dict *diccionario = new Dict ();
	diccionario->update (new String ("nombre"), new String ("Diego"));
	diccionario->update (new String ("edad"), new Integer (21));
	diccionario->update (True, True);
	diccionario->update (new String ("dinero"), new Float (1600.01));
	diccionario->update (new String ("informacion inecesaria"), array);
	print (diccionario);
	Os::System (new String ("toilet -fpagga --gay Hola"));
	print (Platform::System ());
	print ((*diccionario) [new String ("nombre")]);
	print ((*diccionario) [new String ("informacion inecesaria")]);
	List *res = (List *) (*diccionario) [new String ("informacion inecesaria")];
	((Prueba *)((*((List *) (*diccionario) [new String ("informacion inecesaria")])) [new Integer (4)]))->si ();
	print (Os::Getcwd ());
	print (new String ("PID:"), Os::Getpid ());
	print (Random::Choice (array));
	Webbrowser::Chrome->open (new String ("https://www.google.es/"));
	Time::Sleep (new Float (2));
	Webbrowser::Firefox->open (new String ("https://www.youtube.com/"));


	Tuple *tupla = new Tuple (True, new Integer (73), new String ("Funciona"));

	
	print (tupla);
	print (Random::Choice (tupla));
	// List *lprueba = new List (new Float (3.14), new Integer (420), new String ("Diego Martín"));
	// print (lprueba);
	return 0;
}
