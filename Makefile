# Defino el archivo destino
TARGET  :=main
# Variable con todas las dependencias (Los archivos .cpp con la extension .o)
DEPS    :=$(patsubst %.cpp,%.o, $(wildcard *.cpp))

# Defino el nombre del compilador
CC      :=g++
# Defino los parámetros
CFLAGS  :=-g
LDFLAGS :=

$(TARGET): $(DEPS)
	# ($^ se refiere a las dependencias) ($@ se refiere a el objetivo (TARGET))
	# Crea una línea con el compilador, las opciones, el nombre de las dependencias -o nombre del objetivo
	#                       g++         -g            main.o interfaz.o             -o lista
	$(CC) $(CFLAGS) $^ -o $@ $(LDFLAGS)

# Para generar los .o dependiendo de los .cpp
%.o: %.cpp
	# Compilador Opciones -c ($< es el objetivo (%.o))
	# g++        -g       -c main.o interfaz.o
	$(CC) $(CFLAGS) -c $<


# Indica que clean no tiene dependencias
.PHONY: clean

clean:
	# Borra todos los .o
	$(RM) *.o
	$(RM) $(TARGET)
	$(RM) *.exe